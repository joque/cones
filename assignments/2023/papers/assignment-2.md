# Assignment

- Course Title: **Software Tools for Data Science**
- Course Code: **STD912S**
- Assessment: Second Assignment
- Released on: 02/10/2023
- Due Date: 20/10/2023

# Problem

## Problem Description

Consider the sales analytics of a Department store as a decision support tool. At scheduled instants of the day, a **consolidated** view of the sales is generated for all the stores. In addition, sales activities in between the consolidated views are also processed. While the former offers a global view of the sales for all stores, the latter helps make punctual decisions for a specific store.

In this assignment, your task is to install and set up a data pipeline to run the said analytics. The pipeline should allow the generation of the consolidated view and the processing of real-time events. Furthermore, you will implement the generation of the consolidated view. Finally, given the list of items in a store for real-time events, you will show the items that sold the least in that store for prompt action. 

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Installation of the data pipeline. (55%)
- Implementation of all views. (35%)
- Evidence of healthy cluster and correct execution of analytics. (10%)

# Submission Instructions

- This assignment is to be completed by a group of at most three students.
- For each group, all documentation should be submitted to a repository created on [Gitlab](https://about.gitlab.com).
- The submission date is Friday, October 20 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Groups that fail to submit on time will face a penalty (5 marks per day).
- A group which fails to submit will be awarded the mark 0.
- Each group is expected to present the project at an agreed date after the submission deadline.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions will be awarded the mark 0, and each student will receive a warning.
- Each group is expected to communicate their details by Thursday, October 5 2023, so that their credentials and the address of the nodes are communicated by that date.
