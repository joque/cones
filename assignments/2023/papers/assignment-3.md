# Assignment

- Course Title: **Software Tools for Data Science**
- Course Code: **STD912S**
- Assessment: Third Assignment
- Released on: 30/10/2023
- Due Date: 15/11/2023

# Problem

## Problem 1

Write a Cuda kernel program in the Julia programming language to multiply two matrices following the method discussed in class.

## Problem 2

Consider the result.csv dataset accessible from [Kaggle](https://www.kaggle.com/datasets/martj42/international-football-results-from-1872-to-2017?select=results.csv). Write  Cuda programs in the Julia programming language that answer the following queries:

- Given the considered period, how often does a team win while away?
- What are the highest home and away scores?

## Assessment Criteria

We will follow the criteria below to assess the problem:

- Problem 1 (40%)
- Problem 2 (60%)

Note that for Problem 2, for each query correctly addressed 50% of the marks will be awarded.

# Submission Instructions

- This assignment is to be completed by a group of at most two students.
- For each group, all documentation should be submitted to a repository created on [Gitlab](https://about.gitlab.com).
- The submission date is Wednesday, November 15 2023, at midnight. Please note that _commits_ after that deadline will not be accepted. Therefore, a submission will be assessed based on the repository's clone at the deadline.
- Groups that fail to submit on time will face a penalty (5 marks per day).
- A group which fails to submit will be awarded the mark 0.
- Each group is expected to present the project at an agreed date after the submission deadline.
- In the case of plagiarism (groups copying from each other or submissions copied from the Internet), all submissions will be awarded the mark 0, and each student will receive a warning.
