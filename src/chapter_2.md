# Chapter 2

This chapter presents cluster computing. Cluster computing is a topic of **distributed computing**. It focuses on how to assemble several machines and perform some advanced computation. In the context of big data or data science, cluster computing studies how to use a network of computer to speed up **data processing**. In this chapter, we discuss the imotivations behind this paradigm shift, explore a few examples and focus on specific tools.

## Motivations

There are various motivations behind moving a data infrastructure to a cluster environment. Here, we discuss two of these motivatitions. First, the relational model, despite its successes, is tailored for transactional operations. However, when facing data operations not organised within a transaction, with large volumes of data and at a higher velocity, the relational model proves inadequate. Furthermore, data is usually stored on disk and loaded in memory for processing. Consequently, large volumes of data increases the input-output (I/O) operations and hinders the overall cost of data processing. It might be more beneficial to keep the data in memory longer than it is usually done.

To address these limitations, a solution might be to scale up; extend the capabilities (memory, processing, storage) of the server handling the data operations. But, this approach only represents a temporaty fix to the problem. An alternative is to scale out: to turn to extendible collection of commodity hardware, connect them via a network and deploy a cluster to support the data pipeline.

## Overview of Cluster Compuping

The transition of data processing from a centralised to a cluster environment requires some adjustments. Indeed, distributing the data and thus the computation on several nodes in the cluster requires another programming model. The most common approach is the **Map/Reduce** model. As the name suggests, Map/Reduce is organised in two phases. First, the **Map** phase, where a __parallelisable__ part of the computation is executed on all the nodes where the data has been distributed. Once the Map  phase completes, all the intermediate results are then aggregated into the final result: the **Reduce** phase. You can read more about Map/Reduce in [[1]](#1). The Map/Reduce model can be implemented with a tool like [_Hadoop_](https://hadoop.apache.org).

The Map/Reduce model is acutally suitable for a **batch** execution: gather all the data in one or several files and delay the execution until a scheduled time. Note that during the execution, no invervention is required between the various commands. However, although a batch processing proves useful in many use cases, it is inadequate in other cases. For example, when the decision makers in an organisation wish to see the sales trends in real-time. In such a case, the processing can no longer be delayed. Here, we can turn to [_Spark_](https://spark.apache.org). A reader interesed in Spark's inner working is referred to [[2]](#2). Finally, in some cases, the data pipeline is a touch more complex. For example, combining both a batch and real-time layers in the data pipeline. This complex problem has been addressed by introducing an independent architecture: the **Lambda architecture**. It splits the processing architecture in three layers: batch serving and speed.

- the batch layer executes functions at scheduled time and updates its dataset at each iteration, including all new data came ine;
- the speed layer processes fresh data as they are ingested in the system;
- the serving layer merges the results from the two previous layers and provides a real-time and batch views.

The image below illustrates the architecture. For each layer, one can choose an implementation tool to fulfil the expected function.
![image](lambda-example.png)

In the remainder of this chapter, we discuss two tools that provide a cluster environment for data computation.

## Tools
### Apache Spark

Apache Spark is a multi-language tool for processing data. Its main abstraction is the resilient distributed dataset (RDD). An RDD is a distributed immutable collection of objects. The RDDs can be stored in memory and computations applied to them. The second abstraction in Apache Spark is the directed acyclic graph (DAG), which helps transform computation into an acyclic graph before execution.

Apache Spark's architecture uses a master-slave architecture with the following components:

- a driver, which runs a master node and accesses the spark context which contains the core functionality;
- a manager, that manages the cluster and oversees the executions of various jobs;
- worker nodes which hosts the jobs using executors that actually execute the tasks.

To install Apache Spark, one can follow the the instructions at <https://spark.apache.org/downloads.html>. In this document, we will take a different approach to installing Apache Spark. We will use [Ansible](https://www.ansible.com) to automate the installation and cofiguration of an Apache Spark standalone cluster.

First, you need to install Ansible on the machines in your cluster. The installation steps for Cent OS 7 are listed below

```bash
yum update

yum install epel-release

yum install ansible
```
Next, create a user on each machine where Ansible will install a Spark instance. Note that the same ansible user should be created on all machines. 

```bash
sudo adduser cbote

sudo passwd cbote
```
Finally, generate SSH keys on each host for the user and copy the public key from the lead node (the one hosting the master instance) to all other nodes.

```bash
ssh-keygen -t rsa -b 8192

ssh-copy-id username@remote-host
```

We will install the Spark cluster on four nodes: 196.216.167.101, 196.216.167.102, 196.216.167.103 and 196.216.167.104. The inventory in our Ansible setup is defined as follows:

```bash
ansible_connection=ssh
ansible_become=true
gather_facts=True
gathering=smart
install_temp_dir=/tmp/ansible-install
spark_history_dir=/tmp/spark-events
python_version=2

[master]
196.216.167.101   ansible_host=196.216.167.101   ansible_host_private=196.216.167.101  ansible_host_id=1

[nodes]
196.216.167.102   ansible_host=196.216.167.102   ansible_host_private=196.216.167.102  ansible_host_id=2
196.216.167.103   ansible_host=196.216.167.103   ansible_host_private=196.216.167.103  ansible_host_id=3
196.216.167.104   ansible_host=196.216.167.104   ansible_host_private=196.216.167.104  ansible_host_id=4
~
```
As for the roles, we consider two roles. First, a common role, where we preprare the machines by installing the required packages. Its summary is presented below.

```yml
- name: create install dir
  file: path="{{ install_dir }}" state=directory


- name: create temporary install dir
  file: path="{{ install_temp_dir }}" state=directory


- name: create spark history server dir
  file: path="{{ spark_history_dir }}" state=directory

- import_tasks: os-packages.yml

- import_tasks: ssh-config.yml
```

The second and most important role is with regards to installing Spark. The listing below presents a summary of the role.

```yml
spark:
  version: 2.2.0
  hadoop_version: 2.7
  scala_version: 2.11
  working_dir: /home/cbote/spark_data
  master_port: 7077
  worker_work_port: 65000
  master_ui_port: 8080
  worker_ui_port: 8085
  download_location: https://archive.apache.org/dist/spark
  user: "spark"
  user_groups: []
  user_shell: "/bin/false"

  env_extras: {}
  defaults_extras: {}
  defaults_extras_master: {"spark.eventLog.enabled":"true","spark.eventLog.dir":"file://{{ spark_history_dir }}","spark.history.fs.logDirectory":"file://{{ spark_history_dir }}"}
  defaults_extras_nodes: {}
```
The overall playbook is presented below:
```yml
- name: spark setup
  hosts: all
  remote_user: cbote
  roles:
    - role: common
    - role: spark
```

All the files used in the Ansible configuration can be found in the spark_conf folder under the repository. Once the configuration is completed, run the playbook as follows.

```bash
ansible-playbook become_user cbote -i /path/to/inventory /path/to/playbook 
```
When the installation completes, one can check the overall health of the cluster through the dashboard presented below. 
![image](apache-spark-dashboard.png)

Next, we can run a few examples in Spark. The first and most common example is SparkPi. To run it, we will use the spark-submit command as follows. Note that you need to change directory to Spark's installation folder.

```bash
./bin/spark-submit --master spark://196.216.167.101:7077 --deploy-mode client --supervise  --class org.apache.spark.examples.SparkPi --conf spark.standalone.submit.waitAppCompletion=true examples/jars/spark-examples_2.13-3.4.1.jar 10
```

Another common example is the word count example. It counts words from a file or a collection and returns the occurrences of each word. For this example, we will use spark-shell, which can be found in the same folder. To start the shell, execute the command below:

```bash
./bin/spark-shell local[4]
```

Then, execute the code below. Note that for our example, we use the [Scala](https://www.scala-lang.org), Spark's default programming language. 

```scala
val map = sc.textFile("/home/cbote/egzample.txt").flatMap(line => line.split(" ")).map(word => (word,1));
val counts = map.reduceByKey(_ + _);
counts.saveAsTextFile("/home/cbote/count-output");
```
Once the example executes, the result can be checked in the count-output folder. A variant of the example is presented below. The main difference here, is that we use a collection instead of a file like in the previous example. 

```scala
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

val spark = SparkSession.builder().master("local[3]").appName("SparkByExamples.com").getOrCreate()

val data = Array("Project Gutenberg", "Alice Adventures in Wonderland", "Project Gutenberg", "Adventures in Wonderland", "Project Gutenberg")

val rdd = spark.sparkContext.parallelize(data)
val rdd2 = rdd.flatMap(f=>f.split(" "))
val rdd3:RDD[(String,Int)]= rdd2.map(m=>(m,1))
val rdd4 = rdd3.reduceByKey(_ + _)
val rdd5 = rdd4.map(a=>(a._2,a._1)).sortByKey()

rdd5.foreach(println)
```
The last example is a streaming example. The main difference between streaming and batch execution (see the previous three examples) is that the data is not gathered at once. In real time, as events occur, they are pushed to the system for processing. 

A few configurations apply for the streaming setup. For example. a Web application or a transactional can push its events to a store (e.g., Cassandra, ArangoDB, etc.) for processing in real time. Similarly, an event log (e.g., [Kafka](https://kafka.apache.org), [Flume](https://flume.apache.org/FlumeUserGuide.html)) can be used to stream events to Spark. In our example, we use Flume. After installing Flume, we define a configuration as below:

```bash
# Agent declaration
a1.sources = src
a1.sinks = snk
a1.channels = chn

# Define source and its properties
a1.sources.src.type = spooldir
a1.sources.src.spoolDir = /home/sysdev/str_data

# Define sink and its properties
a1.sinks.snk.type = file_roll
a1.sinks.snk.channel = chn
a1.sinks.snk.sink.directory = /var/log/tete
a1.sinks.snk.sink.pathManager.extension = json
a1.sinks.snk.sink.rollInterval = 0

# Channel configuration
a1.channels.chn.type = memory
a1.channels.chn.capacity = 10000

# Attaching channels to the source and sink
a1.sources.src.channels = chn
a1.sinks.snk.channel = chn
```
To run Flume, we execute the following command:

`flume-ng agent -n a1 --conf ~/flume/conf/ -f flm_conf/flumeeg.conf -Dflume.root.logger=INFO,console &`

Our streaming example pulls movie ratings from [MovieLens](https://grouplens.org/datasets/movielens/100k/) and determines the highest rated movie in the last 10 seconds.

A sample of our implementation can be seen below

```scala

def main(args : Array[String]): Unit = {
    val sparkConf = new SparkConf().setMaster("local[*]").setAppName("MovieAna")
    val sc = new SparkContext(sparkConf)
    sc.setLogLevel("ERROR")
    val ssc = new StreamingContext(sc, Seconds(15))
    val filestream = ssc.textFileStream("/home/cbote/my_data_dir/")

    filestream.foreachRDD(rdd => {println(rdd.count())})

    // to be completed...

    ssc.checkpoint("/home/cbote/checkpoint")
    ssc.start()
    ssc.awaitTermination()
  }

```
Basically, the code creates streaming context and then establishes a stream from a folder. Whenever a new file is dumped in the folder, the stream processes it. To run the streaming example, use the spark-submit command as below. MovieAna is the name of the object being executed, and the jar file contains all necessary files for the execution.
`./bin/spark-submit --class MovieAna --master local[*]  /home/cbote/movie-analytics/target/scala-2.11/movie-analytics-assembly-0.1.0-SNAPSHOT.jar`

Your task is to complete this example in a language compatible with Spark.

## Hadoop

Coming soon...


## References

<a id="1">[1]</a>
Li, Junhao and Zhang, Hang (2019).
Blaze: Simplified High Performance Cluster Computing.
arXiv Pre Printing

<a id="2">[2]</a>
Zaharia, Matei and Chowdhury, Mosharaf and Franklin, Michael J. and Shenker, Scott and Stoica, Ion (2010)
Spark: Cluster Computing with Working Sets.
HotCloud'10: Proceedings of the 2nd USENIX conference on Hot topics in cloud computing.

