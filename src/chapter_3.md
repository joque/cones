# Chapter 3

In this chapter, we discuss **parallel computing**. The main difference between **parallel computing** and **distributed computing** (e.g., **cluster computing**) is that with parallel computing one does not require multiple nodes. However, there is a need for **multiple processors** to execute the sub-parts of a complex task in parallel. Here, we will essentially focus on __graphics processing unit__ (__gpu__) programming.

Besides __gpus__, __cpus__ are also responsible for executing computation. However, cpus, in fact each core within a cpu is responsible for execution a unit of computation at a time (serial computing). By way of comparison, one can find 8 to 10 cores in a cpu, while a gpu contains hundreds of cores. Common use cases where gpus are useful include gaming, machine learning, etc.

## From Sequential to Parallel computation
### Example 1 -- Matrix Multiplication

Consider two nxn matrices A and B, compute AB (the multiplication of A and B) sequentially. Propose a parallel version to the computation.

$$
A = 
\begin{bmatrix}
1 & 2 & 3 & 0 \\[0.3em]
4 & 3 & 2 & 5 \\[0.3em]
7 & 1 & 3 & 6 \\[0.3em]
5 & 3 & 9 & 2
\end{bmatrix}
\quad \text{ and } \quad
B = 
\begin{bmatrix}
1 & 2 & 3 & 0 \\[0.3em]
4 & 3 & 2 & 5 \\[0.3em]
7 & 1 & 3 & 6 \\[0.3em]
5 & 3 & 9 & 2
\end{bmatrix}
$$

### Example 2 -- Query execution

Consider a dataset with the following features: model, year, color and dealer and price. Given a query, propose a parallel execution for the query. Examples of queries are:

- model = "Corolla" and year > 2001 and color = "green" or "red"
- model = "Corolla" or "Civic" or year < 2001

## Implementation
### Basic vector Examples

First we take two vectors of floating point values and add them on the CPU.

```julia
N = 2^10
x = fill(1.0f0, N)
y = fill(2.0f0, N)

using Test

@test all(y .== 3.0f0)

```

Next, we attempt a parallelisation on the CPU as follows.

```julia
function seq_add!(y, x)
    for i in eachindex(y, x)
        @inbounds y[i] += x[i]
    end
    return nothing
end

function para_add!(y, x)
    Threads.@threads for i in eachindex(y, x)
        @inbounds y[i] += x[i]
    end
    return nothing
end
```
Now we can run both functions and measure the execution time.
```julia
fill!(y, 2)
seq_add!(y, x)
@test all(y .== 3.0)


fill!(y, 2)
para_add!(y, x)
@test all(y .== 3.0)
```

Finally, we can measure the execution time in both cases.
```julia
using BenchmarkTools

@btime seq_add!($y,$x)
@btime para_add!($y, $x)
```
To move to a GPU implementation you need to use a GPU library such as **Cuda**. If you are using a Mac with the M1 processor, you can use the **Metal** library.

```julia
using BenchmarkTools
using Metal

N = 2^10
x = ones(N)
y = zeros(N)
y .= 2

x1 = MtlArray(x)
y1 = MtlArray(y)

z1 = MtlArray{Float64}(undef, N)
function a_par_add(a,b,c)
    for i in 1:length(a)
        @inbounds c[i] = a[i] + b[i]
    end
    return nothing
end

a_par_add(y1,x1,z1)
@btime a_par_add($y1,$x1,$z1)

# move back to the CPU
z = Array(z1)
```

We can also take advantage of the power of GPU arrays. For example, a matrix multiplication can be written as follows:

```julia
a1 = MtlArray(rand(Float32, N, N))

b1 = MtlArray(rand(Float32, N, N))

c1 = a1 * b1

# check the execution time

# same operation on the CPU
a = rand(Float32, N, N)

b = rand(Flaot32, N, N)

c = a * b
```
Note: Provide a corresponding CUDA version

This operation can be re-written manually using the GPU kernel instead of the Array functionality. Homework!

